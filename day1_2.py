#!/bin/env python3
import csv

with open("input1_day1.txt") as f:
    readcsv = csv.reader(f)
    vals = []
    for r in readcsv:
        vals.append(int(r[0]))
        if len(vals) > 1:
            for i in range(len(vals)-1):
                for j in range(1, len(vals)-1):
                    if i != j:
                        if vals[-1] + vals[i] + vals[j] == 2020:
                            print("Result ", vals[-1] * vals[i] * vals[j])
                            exit()
