#!/bin/env python3
from collections import Counter

with open("input1_day2.txt") as f:
    valids = 0
    for line in f.readlines():
        minimun = int(line.split("-")[0])
        maximum = int((line.split("-")[1]).split(" ")[0])
        letter = (line.split(" ")[1]).split(":")[0]
        password = line.split(":")[1]

        # solution from - https://stackoverflow.com/questions/23240969/python-count-repeated-elements-in-the-list#23240989
        count = Counter(list(password))
        if (count[letter] >= minimun) and (count[letter] <= maximum):
           valids += 1
           print("Password Valid: ", line, count)

print("Number of valid passwords ", valids)
