#!/bin/env python3
with open("input1_day2.txt") as f:
    valids = 0
    for line in f.readlines():
        first = int(line.split("-")[0]) - 1
        second = int((line.split("-")[1]).split(" ")[0]) - 1
        letter = (line.split(" ")[1]).split(":")[0]
        password = line.split(":")[1]
        password = password.replace(" ","")

        if (password[first] == letter and password[second] != letter) or (password[first] != letter and password[second] == letter):
           valids += 1
           print("Password Valid: ", line)

print("Number of valid passwords ", valids)
