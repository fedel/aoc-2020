# AoC 2020

https://adventofcode.com/2020

## Day 1 - part 1

[day1_1.py](https://gitlab.com/fedel/aoc-2020/-/blob/master/day1_1.py)

No comments
## Day 1 - part 2 

[day1_2.py](https://gitlab.com/fedel/aoc-2020/-/blob/master/day1_2.py)

No comments

## Day 2 - part 1 

[day2_1.py](https://gitlab.com/fedel/aoc-2020/-/blob/master/day2_1.py)

I sent the wrong answer 2 times, the first one because I sent the number of 
invalids, and the second because I didn't use <= and >= so the number of
valids were lower than it should be.


## Day 2 - part 2 

[day2_2.py](https://gitlab.com/fedel/aoc-2020/-/blob/master/day2_2.py)

I had to change the code to ignore remove white spaces from passord, and it is
important to use index minus one (as python and almost all languages start 
indexing from 0)
